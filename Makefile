ARCHS = arm64

include $(THEOS)/makefiles/common.mk

APPLICATION_NAME = abc
abc_FILES = main.m AppDelegate.m RootViewController.m iCarousel.m Colours.m
abc_FRAMEWORKS = UIKit CoreGraphics QuartzCore AVFoundation
abc_CFLAGS = -fobjc-arc

include $(THEOS_MAKE_PATH)/application.mk

after-install::
	install.exec "killall \"abc\"" || true
