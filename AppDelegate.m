#import "AppDelegate.h"
#import "RootViewController.h"

@implementation AppDelegate

//@synthesize window;
//@synthesize viewController;

- (void)applicationDidFinishLaunching:(UIApplication *)application {

	_window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
	
	_window.rootViewController = [[RootViewController alloc] init];
	
	[_window makeKeyAndVisible];
}

/*
- (void)dealloc {
	[_window release];
	[_rootViewController release];
	[super dealloc];
	
	
	
	
}*/

@end
