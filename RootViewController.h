#import <UIKit/UIKit.h>


typedef NS_ENUM(NSInteger, languageID) {
    languageIDEnglish = 1,
    languageIDArabic
};  

@interface RootViewController : UIViewController


@property (nonatomic, assign) languageID lang;

@end



