#import "RootViewController.h"
#import "iCarousel.h"
#import "Colours.h"
#import <AVFoundation/AVFoundation.h>
#import <QuartzCore/QuartzCore.h>



@interface RootViewController () <iCarouselDataSource, iCarouselDelegate, UIActionSheetDelegate>

@property (nonatomic, strong) iCarousel *carousel;

@property (nonatomic, strong) NSMutableArray *items;

@property (nonatomic) NSArray *lettersEN;

@property (nonatomic) NSArray *wordsEN;

@property (nonatomic ,strong) NSMutableArray *lettersAR;

@property (nonatomic) NSArray *wordsAR;

@property (nonatomic) AVSpeechSynthesizer *synthesizer;

@property (nonatomic) AVSpeechUtterance *speechutt;

@property (nonatomic, strong) NSMutableArray *images;

@property (nonatomic, strong) UIButton *changeLang;

@property (nonatomic, strong) UIButton *secBtn;

@property (nonatomic, strong) NSMutableDictionary *main;

@property (nonatomic) BOOL didRunBefore;


@end

@implementation RootViewController 

@synthesize lettersEN, synthesizer, speechutt, images, wordsEN, lettersAR, wordsAR, main, changeLang, didRunBefore, secBtn, lang;

- (BOOL)prefersStatusBarHidden
{
	return true;
}


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
	if ((self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil])) {
	
		NSArray *imagePaths = [[NSBundle mainBundle] pathsForResourcesOfType:@"jpg" inDirectory:@"cards"];
	
		images = [[NSMutableArray alloc] init];
		for (NSString *path in imagePaths) {
			[images addObject:[UIImage imageWithContentsOfFile:path]];
			}


		self.items = [NSMutableArray array];
		for (int i = 1; i < 27; i++) {
			[_items addObject:@(i)];
		}
    }
    return self;
}



- (void)loadView {
	[super loadView];
	
	//call anything before the view is loaded
	// lang 1 = english 
	// lang 2 = arabic
	static NSUserDefaults *defaults;
	defaults = [NSUserDefaults standardUserDefaults];
	
	didRunBefore = [[defaults objectForKey:@"didRunBefore"] boolValue];
	if (!didRunBefore) {
		[defaults setObject:@YES forKey:@"didRunBefore"];
	}
	lang = languageIDArabic;
	
	[self setArrays];
	
}
- (void)setArrays {
	
	
	NSString* wordPath = [[NSBundle mainBundle] pathForResource:@"alpha" ofType:@"plist"];
	
	main = [NSMutableDictionary dictionaryWithContentsOfFile:wordPath];
	
	
	lettersAR = [NSMutableArray arrayWithArray:[main objectForKey:@"lettersAR"]];
	
	
	wordsAR = [NSMutableArray arrayWithArray:[main objectForKey:@"wordsAR"]];
	
	lettersEN = [NSMutableArray arrayWithArray:[main objectForKey:@"lettersEN"]];
	
	wordsEN = [NSMutableArray arrayWithArray:[main objectForKey:@"wordsEN"]];
	
	
}

- (void)dealloc
{
	_carousel.delegate = nil;
	_carousel.dataSource = nil;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
	
	synthesizer = [[AVSpeechSynthesizer alloc]init];
	
	self.view.backgroundColor = [UIColor colorWithRed:0.96 green:0.89 blue:0.73 alpha:1.0];
	
	[self setUpCarousel];
	[self getButtons];
}

- (void)setUpCarousel {
	
	
	//create carousel
	_carousel = [[iCarousel alloc] initWithFrame:self.view.bounds];
	_carousel.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
	_carousel.backgroundColor = [UIColor colorWithRed:0.96 green:0.89 blue:0.73 alpha:1.0];
    _carousel.type = iCarouselTypeInvertedWheel;
    _carousel.centerItemWhenSelected = YES;
	_carousel.delegate = self;
	_carousel.dataSource = self;

	//add carousel to view
	[self.view addSubview:_carousel];
	
	
}


- (void)viewDidUnload
{
    [super viewDidUnload];
    self.carousel = nil;
    images = nil;
    wordsEN = nil;
    lettersAR = nil;
    wordsAR = nil;
    main = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return YES;
}




#pragma mark -
#pragma mark iCarousel methods

- (NSInteger)numberOfItemsInCarousel:(iCarousel *)carousel
{
	int cnt;
	if (lang == 2) {
		
		cnt = [lettersAR count];
	}else{
		cnt = [_items count];
	}
    
    return cnt;
}



- (UIView *)carousel:(iCarousel *)carousel viewForItemAtIndex:(NSInteger)index reusingView:(UIView *)view
{
static UILabel *label = nil;
static UIButton *letterBtn = nil;
static UIButton *wordBtn = nil;
    
    //create new view if no view is available for recycling
    if (view == nil)
    {
        //don't do anything specific to the index within
        //this `if (view == nil) {...}` statement because the view will be
        //recycled and used with other index values later
        view = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 240.0f, 320.0f)];
       ((UIImageView *)view).image = [UIImage imageNamed:@"background.jpg"];
        view.contentMode = UIViewContentModeCenter;
        
        view.clipsToBounds = YES;

		view.layer.cornerRadius = 10;
		view.userInteractionEnabled = YES;
		
		
		
		label = [[UILabel alloc] initWithFrame:CGRectMake(0.0, 0.0, view.frame.size.width, view.frame.size.height)];
		
		label.font = [UIFont systemFontOfSize:80];
		label.tag = 1;
		label.textAlignment = NSTextAlignmentCenter;
		
		[view addSubview:label];
		
		
		
		letterBtn = [UIButton buttonWithType:UIButtonTypeCustom];
		letterBtn.frame = CGRectMake(0.0f, 0.0f, view.frame.size.width, 80);
		[letterBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
		letterBtn.backgroundColor = [UIColor clearColor];
		[letterBtn addTarget:self action:@selector(letterTap:) forControlEvents:UIControlEventTouchUpInside];
		[letterBtn setTitleColor:[UIColor clearColor] forState:UIControlStateNormal];
		
		[view addSubview:letterBtn];
		
		wordBtn = [UIButton buttonWithType:UIButtonTypeCustom];
		wordBtn.frame = CGRectMake(0.0f, 80.0f, view.frame.size.width, view.frame.size.height);
		wordBtn.backgroundColor = [UIColor clearColor];
		[wordBtn addTarget:self action:@selector(wordTap:) forControlEvents:UIControlEventTouchUpInside];
		[wordBtn setTitleColor:[UIColor clearColor] forState:UIControlStateNormal];
		[view addSubview:wordBtn];
       
       
    }
    else
    {
        //get a reference to the label in the recycled view
        label = (UILabel *)[view viewWithTag:1];
    }
    
	
	
	// if lang is english
	if (lang == languageIDEnglish) {
	
	
	
		UIGraphicsBeginImageContextWithOptions(((UIImageView *)view).frame.size, NO, 1.0f);

    //draw
    [images[index] drawInRect:CGRectMake(0.0f, 0.0f, ((UIImageView *)view).frame.size.width, ((UIImageView *)view).frame.size.height)];

    //capture resultant image
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
     ((UIImageView *)view).image = image;
	}else if (lang == languageIDArabic) {
		// if lang is arabic
		
		label.text = lettersAR[index];
		
	}
     
    
    //label.font = [UIFont fontWithName:@"Courier-Bold" size:70];
    
    return view;
}






#pragma mark -
#pragma mark view tap event


- (void)wordTap:(UIButton *)sender {
	
	
	NSInteger index = [_carousel indexOfItemViewOrSubview:sender];
	
	
	
	
	if (!synthesizer.speaking) {
		if (lang == languageIDEnglish) {
			speechutt = [AVSpeechUtterance speechUtteranceWithString:wordsEN[index]];
			speechutt.voice = [AVSpeechSynthesisVoice voiceWithLanguage:@"en-us"];
		}else if (lang == languageIDArabic) {
			speechutt = [AVSpeechUtterance speechUtteranceWithString:wordsAR[index]];
			speechutt.voice = [AVSpeechSynthesisVoice voiceWithLanguage:@"ar-sa"];
		}
		
		
	[speechutt setRate:0.4f];
	speechutt.pitchMultiplier = 1.32f;
	
	
	[synthesizer speakUtterance:speechutt];
	}
	
	

	
	
	
}

- (void)letterTap:(UIButton *)sender
{
	//get item index for button
	
	NSInteger index = [_carousel indexOfItemViewOrSubview:sender];
	
	if (!synthesizer.speaking) {
		if (lang == languageIDEnglish) {
			speechutt = [AVSpeechUtterance speechUtteranceWithString:lettersEN[index]];
			
				speechutt.voice = [AVSpeechSynthesisVoice voiceWithLanguage:@"en-us"];
		}else if (lang == languageIDArabic) {
			speechutt = [AVSpeechUtterance speechUtteranceWithString:lettersAR[index]];
				speechutt.voice = [AVSpeechSynthesisVoice voiceWithLanguage:@"ar-sa"];
		}
	
	[speechutt setRate:0.5f];

	
	[synthesizer speakUtterance:speechutt];
	
	}
}


- (CGFloat)carousel:(iCarousel *)carousel valueForOption:(iCarouselOption)option withDefault:(CGFloat)value
{
    //customize carousel display
    switch (option)
    {
    	case iCarouselOptionWrap:
        {
            //normally you would hard-code this to YES or NO
            return NO;
        }
        
        case iCarouselOptionSpacing:
        {
            //add a bit of spacing between the item views
            return value * 1.1f;
        }
        
        default:
        {
            return value;
        }
    }
}
 
#pragma mark -
#pragma mark Buttons

- (void)chLA:(UIButton *)sender
{
	
	
	
	
	//fade in
    [UIView animateWithDuration:0.5f animations:^{

        [_carousel setAlpha:0.0f];
        
        

    } completion:^(BOOL finished) {

		if (lang == 1) {
		[sender setTitle:@"Arabic" forState:UIControlStateNormal];
		lang = 2;
		[_carousel reloadData];
	}
	 else {
		[sender setTitle:@"English" forState:UIControlStateNormal];
		
		lang = 1;
		[_carousel reloadData];
	}
        //fade out
        [UIView animateWithDuration:0.5f animations:^{

            [_carousel setAlpha:1.0f];

        } completion:nil];

    }];
}
	
	
	

- (void) secAC:(UIButton *)sender 
{

}

- (void) getButtons{
	
	
	
	
	NSArray *cS = [[UIColor seafoamColor] colorSchemeOfType:ColorSchemeTriad];
	
	
	changeLang = [UIButton buttonWithType:UIButtonTypeCustom];
	changeLang.frame = CGRectMake(0.0f, 0.0f, self.view.frame.size.width, 50);
		changeLang.backgroundColor = cS[0];
		[changeLang setTitleColor:[UIColor seafoamColor] forState:UIControlStateNormal];
	[changeLang addTarget:self action:@selector(chLA:) forControlEvents:UIControlEventTouchUpInside];
	
	if (lang ==1) {
		[changeLang setTitle:@"English" forState:UIControlStateNormal];
	}else {
		[changeLang setTitle:@"Arabic" forState:UIControlStateNormal];
	}
	
	
	
	
	[_carousel addSubview:changeLang];
	
	
	// adding a Label to the bottom 

	
	
	
	UILabel *foot = [[UILabel alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height - 50, self.view.frame.size.width, self.view.frame.size.height)];
	
	foot.textColor = [UIColor seafoamColor];
	
	foot.backgroundColor = cS[0];
	
	foot.text = @"Wh0ba 2018";
	
	foot.textAlignment = NSTextAlignmentCenter;
	
	
	[_carousel addSubview:foot];
	
	
	
}






@end
